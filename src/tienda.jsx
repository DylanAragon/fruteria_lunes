import React from 'react';
import styled from 'styled-components';

const Producto = styled.div`

background-color: #2EC310;
color: white;
border: 1px solid grey;
margin: 5px;
padding 15px;
width: 45%;
font-weight: bold;

`
const Boton = styled.button`

float:right;
background-color: red;
padding: 4px;

`



export default (props) => {

    const Mostrador = props.frutillas.map((el) => (
        <Producto key={el.id}>{el.nom} ({el.preu}€/u)<Boton onClick={() => props.afegir(el.id)}>Añadir</Boton></Producto>
    ));

    return(
        <>
        {Mostrador}
        </>
    );
}