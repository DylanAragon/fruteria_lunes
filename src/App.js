import React, { useState } from 'react';
import './App.css';
import Lista from './tienda.jsx';
import frutas from './frutas.json';
import Carro from './compra.jsx';


function App() {

  const [listaCompra, setListacompra] = useState(frutas);

  const afegir = (id) => {
    const nuevaLista = listaCompra.map(
      el => {
        if (el.id === id) {
          el.unit = el.unit + 1;
        } return (el);
      })
    setListacompra(nuevaLista)
  }

  const eliminar = (id) => {
    const nuevaLista = listaCompra.map(
      el => {
        if (el.id === id) {
          el.unit = 0;
        } return (el);
      })
    setListacompra(nuevaLista)
  }

  return (
    <>
      <Lista frutillas={listaCompra} afegir={afegir} />
      <Carro frutillas={listaCompra} borrar={eliminar} />
    </>
  );
}

export default App;
