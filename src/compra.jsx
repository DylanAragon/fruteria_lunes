import React from 'react';
import styled from 'styled-components';

const Carro = styled.div`

color: black;
border: 1px solid black;
margin: 5px;
padding 15px;
width: 45%;
font-weight: bold;

`
const Boton = styled.button`

float:right;
background-color: red;
padding: 4px;

`

export default (props) => {

    

    const Ticket = props.frutillas.filter((el) => el.unit > 0).map((el) => (
        <Carro key={el.id}>{el.nom} ({el.preu}€/u) Unidades: {el.unit} Preu total: {el.unit * el.preu}€<Boton onClick={() => props.borrar(el.id)}>eliminar</Boton></Carro>
    ));

    

    return (
        <>
            {Ticket}
        </>
    )
}